package br.up.edu.e1app05;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void mostrar(View v){

        EditText caixaNumero = (EditText) findViewById(R.id.txtNumero);
        EditText caixaMes = (EditText) findViewById(R.id.txtMes);

        String txtNumero = caixaNumero.getText().toString();

        int numero = Integer.parseInt(txtNumero);

        switch (numero){
            case 1:
                caixaMes.setText("Janeiro");
                break;
            case 2:
                caixaMes.setText("Fevereiro");
                break;
            case 3:
                caixaMes.setText("Março");
                break;
            case 4:
                caixaMes.setText("Abril");
                break;
            case 5:
                caixaMes.setText("Maio");
                break;
            case 6:
                caixaMes.setText("Junho");
                break;
            case 7:
                caixaMes.setText("Julho");
                break;
            case 8:
                caixaMes.setText("Agosto");
                break;
            case 9:
                caixaMes.setText("Setembro");
                break;
            case 10:
                caixaMes.setText("Outubro");
                break;
            case 11:
                caixaMes.setText("Novembro");
                break;
            case 12:
                caixaMes.setText("Dezembro");
                break;
            default:
                caixaMes.setText("Inválido");
                break;
        }

    }
}






